// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Resource from 'vue-resource'
import * as fb from 'firebase'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import './assets/css/font-awesome.min.css'

Vue.use(Resource)
Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
	router,
	store,
  components: { App },
	template: '<App/>',
	created() {
		fb.initializeApp({
			apiKey: "AIzaSyDE5UVHzjz2OpWYBbkbNl_nfMg9USvvWF8",
			authDomain: "movies-e232e.firebaseapp.com",
			databaseURL: "https://movies-e232e.firebaseio.com",
			projectId: "movies-e232e",
			storageBucket: "movies-e232e.appspot.com",
			messagingSenderId: "768015168642"
		})

		fb.auth().onAuthStateChanged(user => {
			this.$store.dispatch('autoLogin', user)
		})
	}
})
