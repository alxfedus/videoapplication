import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import movies from './movies'
import shared from './shared'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		user,
		movies,
		shared
	},
	strict: process.env.NODE_ENV !== 'production'
})