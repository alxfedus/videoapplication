import Vue from 'vue'
import * as fb from 'firebase'

class Video {
	constructor(id, ownerId, title, description, url, thumbnails, date) {
		this.id = id
		this.ownerId = ownerId
		this.title = title
		this.description = description
		this.url = url
		this.thumbnails = thumbnails
		this.date = date
	}
}

export default {
	state: {
		videos: [],
		myVideos: [],
	},
	mutations: {
		videos(state, {videos}) {
			state.videos = videos
		},
		updateVideos(state, payload) {
			state.myVideos = payload
		}
	},
	actions: {
		async videos({commit}, {search}) {
			const ytApi = 'AIzaSyDE5UVHzjz2OpWYBbkbNl_nfMg9USvvWF8'
			const url = `https://www.googleapis.com/youtube/v3/search?part=snippet
									 &q=${search}&maxResults=6&order=date&type=video
									 &videoCaption=closedCaption&key=${ytApi}`
			commit('clearError')
			commit('setLoading', true)
			try{
				const response = await Vue.http.get(url)
				const data = await response.json()
				const videos = []
				for(let item of data.items) {
					let url = `https://www.youtube.com/embed/${item.id.videoId}`
					const newItem = new Video(item.id.videoId, null, item.snippet.title, item.snippet.description, url, item.snippet.thumbnails, item.snippet.publishedAt)
					videos.push(newItem)
				}
				commit('videos', {videos})
				commit('setLoading', false)	
			} catch(error) {
				commit('setLoading', false)
				commit('setError', error.message)
			}
		},
		async addToVideos({commit, getters, dispatch}, {id}) {
			commit('clearError')
			try {
				const video = getters.getVideo(id)
				const item = {
					...video,
					ownerId: getters.user.id
				}
				await fb.database().ref('videos').push(item)
				dispatch('updateVideos')
			} catch(error) {
				commit('setError', error.message)
				throw error
			}
		},
		async removeFromMyVideos({commit, dispatch}, {id}) {
			commit('clearError')
			try{
				const fbVal1 = await fb.database().ref('videos').once('value')
				const videos = fbVal1.val()
				Object.keys(videos).forEach(key => {
					const video = videos[key]
					if(video.id === id) {
						fb.database().ref('videos').child(key).remove()
					}
				})
				dispatch('updateVideos')
			} catch(error) {
				commit('setError', error.message)
				throw error
			}
		},
		async updateVideos({commit}) {
			commit('clearError')
			const resultVideos = []
			try{
				const values = await fb.database().ref('videos').once('value')
				const videos = values.val()
				if (videos) {
					Object.keys(videos).forEach(key => {
						const video = videos[key]
						resultVideos.push(video)
					})
				}
				commit('updateVideos', resultVideos)
			} catch(error) {
				commit('setError', error.message)
				throw error
			}
		},
	},
	getters: {
		getVideos: state => state.videos,
		getVideo(state) {
			return id => {
				return state.videos.find(video => video.id === id)
			}
		},
		getMyVideos(state) {
			return ownerId => {
				return state.myVideos.filter(video => video.ownerId === ownerId)
			}
		},
		getMyVideo(state) {
			return id => {
				return state.myVideos.find(video => video.id === id)
			}
		}
	}
}