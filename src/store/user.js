import * as fb from 'firebase'

class User {
	constructor (id, email) {
		this.id = id
		this.email = email
	}
}

export default {
	state: {
		user: null
	},
	mutations: {
		registerUser(state, payload) {
			state.user = payload
		}
	},
	actions: {
		async registerUser ({commit}, {email, password}) {
			commit('clearError')
			commit('setLoading', true)
			try {
				const user = await fb.auth().createUserWithEmailAndPassword(email, password)
				commit('registerUser', new User(user.uid, email))
				commit('setLoading', false)
			} catch(error) {
				commit('setLoading', false)
				commit('setError', error.message)
				throw error
			}
		},
		async loginUser ({commit}, {email, password}) {
			commit('clearError')
			commit('setLoading', true)
			try {
				const user = await fb.auth().signInWithEmailAndPassword(email, password)
				commit('registerUser', new User(user.uid, email))
				commit('setLoading', false)
			} catch(error) {
				commit('setLoading', false)
				commit('setError', error.message)
				throw error
			}
		},
		autoLogin({commit}, payload) {
			if(payload) {
				commit('registerUser', new User(payload.uid, payload.email))
			}
		},
		logoutUser({commit}) {
			fb.auth().signOut()
			commit('registerUser', null)
		}
	},
	getters: {
		isUserLogged(state) {
			return state.user !== null
		},
		user(state) {
			return state.user
		}
	}
}