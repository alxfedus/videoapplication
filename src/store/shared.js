export default {
	state: {
		loading: false,
		loading2: false,
		error: null
	},
	mutations: {
		setLoading(state, payload) {
			state.loading = payload
		},
		setLoading2(state, payload) {
			state.loading2 = payload
		},
		setError(state, payload) {
			state.error = payload
		},
		clearError(state) {
			state.error = null
		}
	},
	actions: {
		setLoading ({commit}, payload) {
			commit('setLoading', payload)
		},
		setLoading2 ({commit}, payload) {
			commit('setLoading2', payload)
		},
		setError ({commit}, payload) {
			commit('setError', payload)
		},
		clearError ({commit}) {
			commit('clearError')
		}
	},
	getters: {
		loading (state) {
			return state.loading
		},
		loading2 (state) {
			return state.loading2
		},
		error (state) {
			return state.error
		}
	}
}