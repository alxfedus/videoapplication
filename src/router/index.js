import Vue from 'vue'
import Router from 'vue-router'
import AuthGuard from './auth-guard'
import Home from '@/components/Home'
import SingleVideo from '@/components/SingleVideo'
import Registration from '@/components/Registration'
import Login from '@/components/Login'
import MyVideos from '@/components/MyVideos'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
		},
		{
      path: '/video/:id',
			name: 'Video',
			props: true,
			component: SingleVideo,
			//beforeEnter: AuthGuard
		},
		{
      path: '/registration',
			name: 'Registration',
      component: Registration
		},
		{
      path: '/login',
			name: 'Login',
      component: Login
		},
		{
      path: '/myVideos',
			name: 'MyVideos',
			component: MyVideos
		}
	],
	mode: 'history'
})
